import sys
import os
import pandas as pd

if __name__ == '__main__':
    '''
    Generates csv file for epochs, from given txt.

    Arguments:

        input_txt: path to the txt file.
        output_csv: Optional. Path to save to csv. Defaults to ../target/epochs.csv, relative to the script location.
    '''
    if len(sys.argv) < 2:
        print("Error: Missing input path - no .txt file as input.")
        print(f"Hint: Please provide the input path as a command line argument (python {os.path.basename(sys.argv[0])} <input_path>).")
        sys.exit(1)

    input_txt = sys.argv[1]

    output_csv = ''

    input_txt = os.path.abspath(input_txt)

    if(len(sys.argv)>2):
        output_csv = sys.argv[2]
        output_csv = os.path.abspath(output_csv)
        if(os.path.isdir(output_csv)):
            output_csv = os.path.join(output_csv, 'epochs.csv')
    else:
        output_csv = os.makedirs(os.path.abspath(os.path.join(os.path.dirname(__file__), '../results')), exist_ok = True)
        output_csv = os.path.abspath(output_csv, 'epochs.csv')
    
    results=[]
    standart_len = 6

    with open(input_txt, "r") as txt_file:
        for line in txt_file:
            elements = line.strip().split()
            if(len(elements)<=1):
                continue
            if(len(elements)>standart_len):
                dif_len = len(elements)-standart_len
                tmp_element = ''
                for i in range(dif_len+1):
                    tmp_element += elements[i] + ' '
                
                elements = [tmp_element.strip(), *elements[dif_len+1:]]
            
            if(elements[1]=='Present'):
                elements[1] = 0
            
            if(elements[3]=='Present'):
                elements[3] = 0
        
            elements = {
                'name': elements[0],
                'start': elements[1],
                'end': elements[3],
                'time_metric': elements[4],
                'type': elements[5],
            }

            results.append(elements)
    
    results = pd.DataFrame(results)
    results.to_csv(output_csv, index=False, sep=';')
