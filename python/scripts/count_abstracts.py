import os, os.path
import sys
import subprocess
import pandas as pd

def main(path, relevant_publishers):
    '''
    main function for count_abstracts script. Counts abstracts in the given directory.

    Arguments:

        path (str): directory, where to count abstracts.
        relevant_publishers (str): path to csv file with result from get_list_of_publishers.py

    Returns:
        
        int: Total number of abstracts for given publishers (hard coded to Elsevier and Springer).
    '''
    bash_script_path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), 
            'count_abstracts.sh'
        )
    )

    subprocess.run(['bash', bash_script_path, path])

    relevant_publishers = pd.read_csv(relevant_publishers, delimiter=',', quotechar='"')

    api_publisher = {'Elsevier', 'Springer-Verlag'}

    relevant_publishers_filtered = relevant_publishers[~(relevant_publishers['publisher'].isin(api_publisher) | relevant_publishers['publisher'].str.startswith('Wiley'))]

    sum_occurrences_others = relevant_publishers_filtered['occurrences'].sum()

    print('Elsevier: ', relevant_publishers[relevant_publishers['publisher']=='Elsevier']['occurrences'].sum())
    print('Springer: ', relevant_publishers[relevant_publishers['publisher']=='Springer-Verlag']['occurrences'].sum())
    print('Wiley: ', relevant_publishers[relevant_publishers['publisher'].str.startswith('Wiley')]['occurrences'].sum())
    print('Others: ', sum_occurrences_others)

    return relevant_publishers[relevant_publishers['publisher']=='Elsevier']['occurrences'].sum() + relevant_publishers[relevant_publishers['publisher']=='Springer-Verlag']['occurrences'].sum()

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Error: Missing input paths - no path for the abstracts directory and .csv file with relevant publishers.")
        print(f"Hint: Please provide the input paths as a command line arguments (python {os.path.basename(sys.argv[0])} <abstracts_path> <relevant_publishers_path>).")
        sys.exit(1)

    path = os.path.abspath(sys.argv[1])
    relevant_publishers = os.path.abspath(sys.argv[2])
    main(path, relevant_publishers)
    
    




# print len([name for name in os.listdir('.') if os.path.isfile(name)])