
import os
import sys
import pandas as pd
from dotenv import load_dotenv

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from helpers import get_by_doi

def main(file_path, out_dir, prefix_reference_file='', verbose=False):
    '''
    main function for get_abstracts script. Downloads abstracts from different dourses. Sources hard coded.

    Arguments:

        file_path (str): path to csv file with column doi. List of DOIs, for which abstracts are needed.
        out_dir (str): path to save the result abstracts.
        prefix_reference_file (str): Path to prefix reference file for DOIs.
        verbose (bool): Optional. Vebose. Defaults to False.
    '''

    print('Downloading abstract/papers...')
    
    csvfile = pd.read_csv(file_path, delimiter=';', quotechar='"')
    dois = csvfile['doi']

    prefix_dict = get_by_doi.get_doi_prefix_dict(prefix_reference_file)
    skipped_reqs = []

    for row in range(len(dois)):
        doi = dois[row]
        if doi != '' and type(doi) == str:
            publisher = get_by_doi.get_publisher(doi, prefix_dict)
            if verbose: print(f'📖 { row } ::: {doi} ::: {publisher}')

            if publisher == "Elsevier":
                get_by_doi.get_by_doi(doi, os.path.join(out_dir, "elsevier"), os.environ.get('ELSEVIER_API_KEY'))
            elif publisher == 'Springer-Verlag':
                get_by_doi.get_by_doi(doi, os.path.join(out_dir, "springer"), os.environ.get('SPRINGER_API_KEY'))
            elif publisher.startswith("Wiley"):
                get_by_doi.get_by_doi(doi, os.path.join(out_dir, "wiley"), os.environ.get('WILEY_API_KEY'))
            else:
                res = get_by_doi.get_doi_from_req(doi, os.path.join(out_dir, 'request'))
                if (not res):
                    skipped_reqs.append({publisher: doi})
    
    skipped_reqs = pd.DataFrame(skipped_reqs)
    skipped_reqs.to_csv(os.path.abspath(os.path.join(out_dir, '../skipped_request_dois.csv')), index=False)

if __name__ == '__main__':
    '''
    Gets abstracts from given DOIs using 3 APIs (Elsevier, Springer and Wiley)
    '''
    if len(sys.argv) < 4:
        print("Error: Missing input paths - .csv file with list of DOIs, output directory path, .csv file with the list of DOI prefixes")
        print(f"Hint: Please provide the input paths as a command line arguments (python {os.path.basename(sys.argv[0])} <input_path> <output_path> <reference_list>).")
        sys.exit(1)

    load_dotenv(os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env'))
    # print(os.environ.get('ELSEVIER_API_KEY'))
    # print(os.environ.get('SPRINGER_API_KEY'))
    # print(os.environ.get('WILEY_API_KEY'))

    file_path = os.path.abspath(sys.argv[1])
    out_dir = os.path.abspath(sys.argv[2])
    prefix_reference_file = os.path.abspath(sys.argv[3])
    verbose = False
    if(len(sys.argv)==4 and sys.argv[3]=='-v'):
        verbose = True
    
    main(file_path, out_dir, prefix_reference_file, verbose)
    
    

