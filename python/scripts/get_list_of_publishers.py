import os
import sys
import pandas as pd
import get_by_doi

def main(file_path, out_dir, prefix_reference_file='', verbose=False):
    '''
    main function for get_list_of_publishers. Filters list of publishers, and gives ones, present in the fiven dataset.

    Arguments:
        file_path (str): path to csv file - dataset, with DOIs.
        out_dir (str): path to the dir where to save relevant publishers list.
        prefix_reference_file (str): Path to prefix reference file for DOIs.
        vebose (bool): Optional. Vebose. Defaults to False.
    
    Retunrs:
        str: path to the result csv file.
    '''
    if not verbose: print('Getting Publishers...')
    
    csvfile = pd.read_csv(file_path, delimiter=';', quotechar='"')
    dois = csvfile['doi']

    prefix_dict = get_by_doi.get_doi_prefix_dict(prefix_reference_file)
    
    relevant_publishers = []
    unknown_publishers = dict()
    no_doi = 0

    for prefix, publisher in prefix_dict.items():
        matches = dois.str.contains(f'^{prefix}/', regex=True).sum()
        if verbose: print(f'📖 { prefix } ::: {publisher} ::: {matches}')

        if matches > 0:
            relevant_publishers.append({'prefix': prefix, 'publisher': publisher, 'occurrences': matches})
    
    relevant_publishers = pd.DataFrame(relevant_publishers)
    relevant_publishers.to_csv(os.path.join(out_dir, 'relevant_publishers.csv'), index=False)

    for row in range(len(dois)):
        doi = dois[row]
        if doi != '' and type(doi) == str:
            publisher = get_by_doi.get_publisher(doi, prefix_dict)
            if publisher == 'NIL':
                prefix = doi.split("/")[0]
                if verbose: print(f'{ prefix }')
                unknown_publishers[prefix] = unknown_publishers[prefix] + 1 if prefix in unknown_publishers else 1
        else:
            if verbose: print(doi)
            no_doi += 1
    

    unknown_publishers = pd.DataFrame(list(unknown_publishers.items()), columns=['prefix', 'occurrences'])
    unknown_publishers.to_csv(os.path.join(out_dir, 'unknown_publishers.csv'), index=False)

    relevant_publishers = relevant_publishers['occurrences'].sum()
    unknown_publishers = unknown_publishers['occurrences'].sum()

    print('Total Data: ', len(dois))
    print('Publishers Known: ', relevant_publishers)
    print('Publishers Unknown: ', unknown_publishers)
    print('DOIs unknown:', no_doi)
    print('Total Checked: ', relevant_publishers+unknown_publishers+no_doi)

    return os.path.join(out_dir, 'relevant_publishers.csv')

if __name__ == '__main__':
    '''
    Gets list of publishers from the given dataset
    Requeres the dataset path.
    '''

    if len(sys.argv) < 4:
        print("Error: Missing input paths - .csv file with list of DOIs, output directory path, .csv file with the list of DOI prefixes")
        print(f"Hint: Please provide the input paths as a command line arguments (python {os.path.basename(sys.argv[0])} <input_path> <output_path> <reference_list>).")
        sys.exit(1)

    file_path = os.path.abspath(sys.argv[1])
    out_dir = os.path.abspath(sys.argv[2])
    prefix_reference_file = os.path.abspath(sys.argv[3])
    verbose = False
    if (len(sys.argv)==4 and sys.argv[3]=='-v'):
        verbose = True
    
    main(file_path, out_dir, prefix_reference_file, verbose)
    
    