import os
import sys
import pandas as pd
import pymysql
from dotenv import load_dotenv


if __name__ == '__main__':
    '''
    Save result of epoch_csv_gen.py / csv file with epochs, to MySQL/MariaDB database.

    Arguments:
        
        input_path: path to the csv file.
    '''
    if len(sys.argv) < 2:
        print("Error: Missing input path - no .csv file as input.")
        print(f"Hint: Please provide the input path as a command line argument (python {os.path.basename(sys.argv[0])} <input_path>).")
        sys.exit(1)
        
    input_path = sys.argv[1]
    input_path = os.path.abspath(input_path)
    epochs = pd.read_csv(input_path, delimiter=';')

    load_dotenv(os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env'))

    db_params = {
        'host':  os.environ.get('db_host'),
        'user':  os.environ.get('db_user'),
        'password':  os.environ.get('db_pass'),
        'database':  os.environ.get('db_name'),
    }

    db_conn = pymysql.connect(**db_params)
    cursor = db_conn.cursor()

    print(type(cursor))

    cursor.execute('''
        CREATE TABLE IF NOT EXISTS epoch_types (
            id INT AUTO_INCREMENT PRIMARY KEY,
            type VARCHAR(16) UNIQUE
        )
    ''')

    unique_types = epochs['type'].unique()
    for type in unique_types:
        cursor.execute('INSERT INTO epoch_types (type) VALUES (%s)', (type,))
    
    db_conn.commit()

    cursor.execute('''
        CREATE TABLE IF NOT EXISTS epochs (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(32) UNIQUE,
            start_ma DOUBLE,
            end_ma DOUBLE,
            type INT,
            FOREIGN KEY (type) REFERENCES epoch_types(id)
        )
    ''')

    for index, row in epochs.iterrows():
        cursor.execute('SELECT id FROM epoch_types WHERE type = %s', (row['type'],))
        type_id = cursor.fetchone()

        cursor.execute(
            'INSERT INTO epochs (name, start_ma, end_ma, type) VALUES (%s, %s, %s, %s)',
            (row['name'], row['start'], row['end'], type_id)
        )
    
    db_conn.commit()
    db_conn.close()
