import requests
import sys

# url = sys.argv[1]
doi = sys.argv[1]

# Specify headers to mimic a browser request
# headers = {
#     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}

# response = requests.get(url, headers=headers)
# response = requests.get(url)
# final_url = response.url
# print(response)
# print(response.text)
# print(response.text.split('"abstract":')[1].split('\n')[0])

# result = requests.get(final_url, headers=headers)

# print("Final URL:", final_url)
# with open('output.html', 'w', encoding='utf-8') as file:
#     file.write(response.text)

# print("Response saved to 'output.html'")

if __name__ == '__main__':
    '''
    Gets abstracts from a request and slices out the abstract part from the response.

    Arguments:
        
        doi: doi to be downloaded.
    '''

    url = 'https://oa.mg/work/'+doi
    response = requests.get(url)
    if (response.status_code == 200):
        print(response.text.split('"abstract":')[1].split('\n')[0].strip()[1:-1])