#!/bin/bash
# Counts abstracts in the given directory.
if [ -z "$1" ]; then
    echo "Usage: $0 <directory_path>"
    exit 1
fi

abstracts_path="$1"

for subdirectory in "$abstracts_path"/*; do
    if [ -d "$subdirectory" ]; then
        # Print subdirectory name
        echo "Source: $(basename "$subdirectory")"
        
        # Count the number of files in the subdirectory
        num_files=$(find "$subdirectory" -type f | wc -l)
        echo "Number of files: $num_files"
        
        echo "---"
    fi
done