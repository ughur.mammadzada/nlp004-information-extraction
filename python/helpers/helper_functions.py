import os

import pymysql
from dotenv import load_dotenv

import requests

def filepath_to_doi(file_path):
    '''
    Converst file_path to DOI.

    Arguments:
        
        file_path (str): File path.

    Returns:

        str: DOI, extracted from the file name.
    '''
    return os.path.splitext(file_path)[0].split('/')[-1].replace('_', '/')

def doi_to_filename(doi, ext=None):
    '''
    Converst DOI to file name.

    Arguments:

        doi (str): DOI to be converted.
        ext (str): Extension of the file. Defaults to None.
    
    Returns:

        str: result file name.
    '''
    if doi.startswith("doi:"):
        doi = doi[4:]
    doi = doi.replace("/", "_")
    if ext is not None:
        doi = os.path.join(ext, doi + "." + ext)
    return doi

def connect_to_db():
    '''
    Connects to the MySQL/MariaDB database. Uses pymysql.

    Arguments:

        none
    
    Returns:

        pymysql.cursors.Cursor: Cursor to the database connection        
    '''
    load_dotenv(os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env'))

    db_params = {
        'host':  os.environ.get('db_host'),
        'user':  os.environ.get('db_user'),
        'password':  os.environ.get('db_pass'),
        'database':  os.environ.get('db_name'),
    }

    db_conn = pymysql.connect(**db_params)

    return db_conn

def filter_named_entities(named_entities, filter):
    '''
    Filters list of named entity dictionaries based on match of entity group (LOC, PER, ORG) to given filter.

    Arguments:

        named_entities (list): List of named entities, each beaing a dictionary.
        filter: group of named entities, which has to be filtered.
    
    Returns:

        list: List of words, entity group of which match the filter.
    '''
    filtered = []
    for entity in named_entities:
        if (entity['entity_group'] == filter and entity['word'] not in filtered):
            filtered.append(entity['word'])
    return filtered

def crossref_metada_api_call(doi):
    '''
    Gets metadata for given DOI from CrossRef.

    Arguments:

        doi (str): DOI, for which metadata is needed.
    
    Returns:

        http response: response from CrossRef.
    '''
    return requests.get('https://api.crossref.org/works/'+doi)