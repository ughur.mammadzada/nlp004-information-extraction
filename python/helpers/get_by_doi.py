import sys
import csv
import os
import re
import ssl
import subprocess
import xml.etree.ElementTree as ET
from io import BytesIO
from lxml import etree
import requests
import json
import time

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from helper_functions import doi_to_filename


def xml_to_txt(xml_path):
    tree = ET.parse(xml_path)
    root = tree.getroot()
    text = list(root.itertext())
    return "\n".join(text)


def get_springer_by_doi(doi, out_dir, api_key):
    """
        get data from Springer
        see: https://dev.springernature.com/restfuloperations
    """

    json_path = os.path.join(out_dir, doi_to_filename(doi, "json"))
    if not os.path.exists(json_path):
        url = "http://api.springernature.com/metadata/json/doi/" + doi + "?api_key=" + api_key
        response = requests.get(url)

        json_response = json.loads(response.text)
        json_response['apiKey'] = 'yourKeyHere'
        
        with open(json_path, 'w') as output_file:
            json.dump(json_response, output_file, indent=2)


def get_wiley_by_doi(doi, out_dir, api_key):
    """
        get data from Wiley
        see: https://onlinelibrary.wiley.com/library-info/resources/text-and-datamining
    """
    pdf_path = os.path.join(out_dir, doi_to_filename(doi, "pdf"))
    if not os.path.exists(pdf_path):
        headers = {"Accept": "application/vnd.crossref.unixsd+xml"}
        doi_resolve = requests.get("http://dx.doi.org/" + doi, headers=headers)
        try:
            tree = etree.parse(BytesIO(doi_resolve.content))
        except BaseException: # [bare-except]
            print(f"🤯 ::: {doi_resolve.url} ::: {doi_resolve.status_code}")
            return None

        search_term = '//{*}collection[@property="text-mining"]' \
                    + '/{*}item' \
                    + '/{*}resource'
        try:
            collections = tree.findall( search_term )
            text_url = collections[0].text
        except IndexError:
            try:
                collections = tree.findall( search_term.replace('text-mining', 'crawler-based') )
                text_url = collections[0].text
            except IndexError:
                # wild guess
                text_url = f"https://onlinelibrary.wiley.com/doi/pdfdirect/{doi}?download=true"


        with open(pdf_path, 'wb') as file:
            headers = {'Wiley-TDM-Client-Token': api_key}
            response = requests.get(text_url, headers=headers)
            file.write( response.content )

def get_elsevier_by_doi(doi: str, out_dir: str, api_key: str):
    """
        get article metadata and text from big E
    """
    xml_path = os.path.join(out_dir, doi_to_filename(doi, "xml"))
    if not os.path.exists(xml_path):
        url = f"https://api.elsevier.com/content/article/doi/{doi}"
        xml_response = requests.get(url, params={"apiKey": api_key})
        if xml_response.status_code != 200:
            () # breakpoint
        else:
            with open(xml_path, "w", encoding='UTF-8') as f:
                f.write(xml_response.text)
    # curl 'https://api.elsevier.com/content/article/doi/10.1016/j.epsl.2017.03.011?apiKey=d0a1fd53101a8504d0ea26ea570c1254' > elsevier/xml/10.1016_j.epsl.2017.03.011.xml
    # curl -H "Accept: text/plain" 'https://api.elsevier.com/content/article/doi/10.1016/j.epsl.2017.03.011?apiKey=d0a1fd53101a8504d0ea26ea570c1254' > elsevier/txt/10.1016_j.epsl.2017.03.011.txt

def get_by_doi(doi, out_dir, api_key):
    """
        main getter for all DOIs
    """
    dir_name = os.path.basename(out_dir)
    if dir_name == "springer":
        os.makedirs(os.path.join(out_dir, "json"), exist_ok=True)
        get_springer_by_doi(doi, out_dir, api_key)
    elif dir_name == "wiley":
        os.makedirs(os.path.join(out_dir, "pdf"), exist_ok=True)
        get_wiley_by_doi(doi, out_dir, api_key)
    elif dir_name == "elsevier":
        os.makedirs(os.path.join(out_dir, "xml"), exist_ok=True)
        get_elsevier_by_doi(doi, out_dir, api_key)


def get_publisher(doi: str, prefix_dict: dict):
    """
        returns the publisher name by DOI prefix
    """
    prefix = doi.split("/")[0]
    try:
        return prefix_dict[prefix]
    except KeyError:
        return "NIL"

def get_doi_prefix_dict(path=''):
    '''
    Gets doi prefixes to publishers match.

    Arguments:
        path (str): Path to csv file with prefix info. Defaults to empty string. Hard coded to read from '../source/doi-prefix-publishers.csv' relative to the script location.

    Returns:
        
        dict: Dictionary of doi_preffix - publisher pairs.
    '''
    if len(path)==0:
        path = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__), 
                '../abstracts/doi-prefix-publishers.csv'
            )
        )

    path = os.path.abspath(path)
    prefix_dict = dict()
    with open(path, newline='', encoding='UTF-8') as csvfile:
        prefix_resolver = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        for _row in prefix_resolver:
            prefix_dict[_row['prefix']] = _row['publisher']
    
    return prefix_dict

def get_doi_from_req(doi, out_dir, source='https://oa.mg/work/'):
    '''
    Gets abstracts from a request and slices out the abstract part from the response.

    Arguments:
        doi (str): DOI of a paper, abstract of which is searched
        source (str): Base url / source to send the request.
    
    Returns:

        bool: True if success, False if fail.
    '''

    out_dir = os.path.abspath(out_dir)
    os.makedirs(os.path.join(out_dir, "txt"), exist_ok=True)
    txt_path = os.path.join(out_dir, doi_to_filename(doi, "txt"))
    

    if not os.path.exists(txt_path):
        url = source+doi
        response = requests.get(url)
        if (response.status_code == 200 and 'Loading · OA.mg' not in response.text):
            # abstract = response.text.split('"abstract":')[1].split('\n')[0].strip()[1:-1]
            abstract = response.text
            with open(txt_path, "w", encoding='UTF-8') as file:
                file.write(abstract)
            return True
        else:
            return False