import os
import sys
import string
import re
import pyparsing as pp
import nltk
from nltk import word_tokenize, pos_tag, ne_chunk
import spacy 
from transformers import pipeline
import pandas as pd

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from helper_functions import connect_to_db

def get_epochs_number(text):
    '''
    Get epoch information in numeric representation from geological papers (KA, MA, GA).

    Arguments:
        text (str): Input string.
    
    Returns:
        list: List of tuples, each being epoch in format of ('num', [..., 'num'], 'epoch_scale (ka, ma, ga, ...)').
    '''

    epoch_indicators_pattern = '\s+(ka|ma|ga)\b'

    epoch_numbers_patterns = [
        r'\b(\d+)\s+(ka|ma|ga)\b',
        r'\b(\d+)\s+(\d+)\s+(ka|ma|ga)\b',
        r'\b(\d+)-(\d+)\s+(ka|ma|ga)\b',
        r'\b(\d+)–(\d+)\s+(ka|ma|ga)\b',
    ]

    results = []

    for number_pattern in epoch_numbers_patterns:
        pattern = rf'{number_pattern}{epoch_indicators_pattern}'
        substrings = re.findall(number_pattern, text)
        results += substrings
    
    return results

def get_epoch_name(text, epoch_numbers=[]):
    '''
    Get epoch information in epoch name representation from geological papers.

    Arguments:

        text (str): Input string.
        epoch_numbers (list): List of tuples, representing numerical epochs. Defaults to empty list.
    
    Returns:
    
        list: List of strings, each beign epoch name.
    '''

    db_connection = connect_to_db()
    cursor = db_connection.cursor()
    cursor.execute('SELECT name, start_ma, end_ma FROM epochs')
    epochs = cursor.fetchall()
    db_connection.close()

    results = []
    
    for epoch in epochs:
        if epoch[0].lower() in text.lower():
            results.append(epoch[0])
                
    for epoch_number in epoch_numbers:
        numbers = None
        metric = None
        if len(epoch_number)>2:
            numbers = [float(number) for number in epoch_number[:2]]
            metric = epoch_number[2]
        else:
            numbers = [float(epoch_number[0])]
            metric = epoch_number[1].lower()

        if metric != 'ma':
            for i in range(len(numbers)):
                if metric == 'ka':
                    numbers[i] = numbers[i]/1000
                if metric == 'ga':
                    numbers[i] = numbers[i]*1000

        filtered_epochs = [
            epoch[0] for epoch in epochs
            if any(
                float(epoch[1]) >= number >= float(epoch[2])
                for number in numbers
            )
        ]

        results = list(set([*results, *filtered_epochs]))

    return results


def get_key_words(text):
    '''
    Get key words like, volcano, isotop, etc. as a dictionary in form of key_word: True/False.

    Arguments:
        text (str): Input string.

    Returns:
        dictionary: Dictionary indicating if the key_word is in the text or not, in form of True/False.
    '''

    key_words = {
        'volcan': False,
        'isotop': False,
    }

    for key, _ in key_words.items():
        key_words[key] = key in text
    
    return key_words


def dcg(text, grammar):
    """
    Uses ntlk.CFG to perform Definite Clause Grammar analisis on text.

    Arguments:

        text (str): Input text.
        grammar (str): String prolog like grammar definition.
    
    Returns:

        nltk.tree.tree.Tree: An nltk tree with tokens at leaves.
    """

    # grammar = """
    #         S -> NP VP
    #         NP -> Det N | N
    #         VP -> V | V PP | M V | M V PP
    #         Det -> 'the' | 'a' | 'this' | 'that' | 'those' | 'these'
    #         N -> 'rock' | 'volcano' | 'sample' | 'plateau' | 'rocks' 
    #         V -> 'found' | 'collected'
    #         M -> 'was' | 'has been' | 'has' | 'were' | 'have' | 'have been'
    #         PP -> P NP
    #         P -> 'in' | 'near'
    #     """

    grammar = nltk.CFG.fromstring(grammar)

    parser = nltk.RecursiveDescentParser(grammar)

    def allParses(sentence):
        # sentence = sentence.split()
        tokens = nltk.word_tokenize(sentence)
        processed_tokens = []
        for token in tokens:
            if token.isdigit() and len(token) > 1:
                processed_tokens.extend(list(token))
            else:
                processed_tokens.append(token)

        try:
            return parser.parse(processed_tokens)
        except ValueError:
            return None
    
    result = allParses(text)

    return result

def named_entity_spacy(text):
    """
    Uses spaxy en_core_web_sm to identify entities.

    Arguments:

        text (str): Input text.
    
    Returns:

        list: A list, where each item a list, containing the token (part of the text), the token's entity annotation and the token's entity type.
    """

    nlp = spacy.load("en_core_web_sm")
    input_text = nlp(text)

    result = []

    for token in input_text:
        result.append([token.text, token.ent_iob_, token.ent_type_])

    return result

def named_entity_nltk(text):
    """
    Uses nltk ne_chunk to identify entities.

    Arguments:

        text (str): Input text.
    
    Returns:

        nltk.tree.tree.Tree: An nltk tree with tokens at leaves.
    """

    tokenized_text = word_tokenize(text)
    tagged_text = pos_tag(tokenized_text)
    result = ne_chunk(tagged_text)

    return result

def named_entity_bert(text):
    """
    Uses fine-tuned bert (balamurugan1603/bert-finetuned-ner) to search got named entities.

    Arguments:

        text (str): Input text.
    
    Returns:

        list: List where each elent is a dictionary, containing entity_group, score, word/entity, start and end of entity in the text.
    """

    model_checkpoint = "balamurugan1603/bert-finetuned-ner"
    namedEntityRecogniser = pipeline(
        "token-classification",
        model=model_checkpoint,
        aggregation_strategy="simple"
    )

    return namedEntityRecogniser(text)