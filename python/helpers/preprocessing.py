import sys
import string
import re
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer

def clean_new_lines(text):
    """
    Removes new lines from the text.

    Arguments:

        text (str): Input text needed to be cleaned/

    Returns:

        str: String text with no new lines.
    """

    return text.replace('\n', '')

def clean_white_spaces(text):
    """
    Removes redundant white spaces.

    Arguments:

        text (str): Input text needed to be cleaned.

    Returns:

        str: String text with single  white spaces, instead of multiple.
    """

    return ' '.join(text.split())

def lowercase_text(text):
    """
    Turn the text into lowercase.

    Arguments:

        text (str): Input text needed to be turned into lowercase.

    Returns:

        str: Text in lower case.
    """

    return text.lower()

def translate_interval_dash(text):
    """
    Translates dashes, showing intervals (e.g. 100-200) into white spaces.

    Arguments:

        text (str): Input text with dashes.

    Returns:

        str: Text with no dashes.
    """

    pattern = r'(\d+)(-|–)(\d+)'

    def matcher(match):
        return match.group(1) + ' ' + match.group(3)

    return re.sub(pattern, matcher, text)

def remove_puntuation(text):
    """
    Removes punctuation marks from the text.

    Arguments:

        text (str): Input text with punctuation marks.

    Returns:

        str: Text with no punctuation marks.
    """

    translator = str.maketrans('', '', string.punctuation)
    return text.translate(translator)

def tokenize_if_string(text):
    """
    If input is string, tokenizes with word_tokenize from nltk. Used in other functions, when input isn't tokenized list but is string.

    Arguments:

        text (str/list): Input text.

    Returns:

        list: tokenized words
    """

    if(type(text) == str):
        return word_tokenize(text)

    return text

def clean_stop_words(text, language='english'):
    """
    Removes stop words (e.g. the, a, or, do, etc.).

    Arguments:

        text (str/list): Input string with stop words. Can be list, if already tokenized.

        language (str): Language of text and stop words. Defaults to 'english'.

    Returns:

        list: List of strings, each one being words, which are not stop words.
    """

    stop_words = set(stopwords.words(language))
    tokens = tokenize_if_string(text)
    clean_text = [word for word in tokens if word not in stop_words]

    return clean_text

def stem_words(text):
    """
    Stemms words in text.

    Arguments:

        text (str/list): Input text. Can be string or list of strings.

    Returns:

        list: List of strings, each one being a stem of a word.
    """

    tokenized_text = tokenize_if_string(text)

    stemmer = PorterStemmer()

    word_stems = [stemmer.stem(word) for word in tokenized_text]

    return word_stems

def lem_words(text):
    """
    Lemms words in text.

    Arguments:

        text (str/list): Input text. Can be string or list of strings.

    Returns:

        list: List of strings, each one being a lemma of a word.
    """

    tokenized_words = tokenize_if_string(text)

    lemmatizer = WordNetLemmatizer()

    lemmatized_words = [lemmatizer.lemmatize(word) for word in tokenized_words]

    return lemmatized_words

def cut_text_on_phrase(text, phrase):
    """
    Cuts the text at the given phrase.

    Arguments:
        
        text (str): Input text.

        phrase (str): The phrase, at which text has to be cut.

    Returns:

        str: The text from begining until the position of the phrase.
    """

    phrase_index = text.find(phrase)

    if phrase_index!=-1:
        return text[:phrase_index]
    else:
        return text
    
def remove_first_line(text):
    """
    Removes first line from text.

    Arguments:

        text (str): Input text.

    Returns:

        str: The text withput first line.
    """

    lines = text.splitlines()
    if len(lines) > 1:
        return '\n'.join(lines[1:])
    else:
        return ''

def preprocessing_pipeline(text, stop_words_lang='english', keep_stop_words=False):
    """
    Performs preprocessing and cleaning on raw input text

    Arguments:

        text (str):  Text input.

        stop_words_lang (str): Language for stop words. Defaults to 'english'.

        keep_stop_words (bool): Defines if stop words should be kept. Defaults to False

    Returns:

        list: Tokens of words.

        list: Stemmed tokens of words.

        list: Lemmatized tokens of words.
    """

    tmp_text = text

    tmp_text = clean_new_lines(tmp_text)
    tmp_text = clean_white_spaces(tmp_text)
    tmp_text = lowercase_text(tmp_text)
    tmp_text = translate_interval_dash(tmp_text)
    tmp_text = remove_puntuation(tmp_text)

    tokenized = tokenize_if_string(tmp_text)

    if (not keep_stop_words):
        tokenized = clean_stop_words(tokenized, stop_words_lang)

    stemmed_tokens = stem_words(tokenized)
    lemmatized_tokens = lem_words(tokenized)

    return tokenized, stemmed_tokens, lemmatized_tokens






