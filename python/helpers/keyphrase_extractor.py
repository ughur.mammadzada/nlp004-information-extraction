from transformers import (
    TokenClassificationPipeline,
    AutoModelForTokenClassification,
    AutoTokenizer,
)
from transformers.pipelines import AggregationStrategy
import numpy as np

class KeyphraseExtractionPipeline(TokenClassificationPipeline):
        def __init__(self, model, *args, **kwargs):
            super().__init__(
                model=AutoModelForTokenClassification.from_pretrained(model),
                tokenizer=AutoTokenizer.from_pretrained(model),
                *args,
                **kwargs
            )

        def postprocess(self, all_outputs):
            results = super().postprocess(
                all_outputs=all_outputs,
                aggregation_strategy=AggregationStrategy.SIMPLE,
            )
            return np.unique([result.get("word").strip() for result in results])

def extractor(text):
    """
    Uses keyphrase-extraction-kbir-inspec from ml6team to extract keyphrases from text.
    
    Arguments:

        text (str): Input text.
    
    Returns:

        list: A list of strings, each beign a keyphrase recognized from the text.
    """
    model_name = "ml6team/keyphrase-extraction-kbir-inspec"
    extractor = KeyphraseExtractionPipeline(model=model_name)
    keyphrases = extractor(text)
    return keyphrases