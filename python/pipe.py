import sys
import os
import time
import re

import pandas as pd

import xml.etree.ElementTree as ET
import json

from dotenv import load_dotenv

from helpers import rules, preprocessing, helper_functions
from scripts import get_abstracts, count_abstracts, get_list_of_publishers

filepath_to_doi = helper_functions.filepath_to_doi
filter_named_entities = helper_functions.filter_named_entities
crossref_metada_api_call = helper_functions.crossref_metada_api_call

get_abstracts = get_abstracts.main
get_list_of_publishers = get_list_of_publishers.main
count_abstracts = count_abstracts.main

def cleaning_pipe(text):
    '''
    Uses function from preprocessing, to clean text.

    Argumnets:

        text (str): input string.

    Returns:

        str: resulting clean text.
    '''

    result = text
    result = preprocessing.clean_new_lines(result)
    result = preprocessing.clean_white_spaces(result)
    result = preprocessing.translate_interval_dash(result)
    result = preprocessing.remove_puntuation(result)

    return result

def metadata_collector(abstracts, verbose=False):
    '''
    Collects metada from CrossRef.

    Arguments:

        abstracts (list): list of abstracts, for which metadata should be collected.
        vebose (bool): verbose. Defaults to False.
    
    Returns:

        list: list of metadata, including publication date, title, sourse, authors.
    '''
    result = []
    data_length = len(abstracts)

    print('Collecting metadata...')

    for i in range(data_length):
        doi = abstracts[i]['doi']
        if verbose: print(f'{i+1}/{data_length+1}: {doi}')
        response = crossref_metada_api_call(doi)
        tmp_res = {'doi': doi}
        if (response.status_code == 200):
            response = response.json().get('message', {})
            
            publication_date = response.get('published-print', {})
            tmp_res['publication_date']  = publication_date.get('date-parts', [])
            tmp_res['publication_date'] = tmp_res['publication_date'][0] if len(tmp_res['publication_date']) else []
            
            tmp_res['title'] = response.get('title', '')
            tmp_res['source_title'] = response.get('container-title', '')
            tmp_res['source_volume'] = response.get('volume', '')
            tmp_res['source_issue'] = response.get('issue', '')
            tmp_res['source_page'] = response.get('page', '')
            
            tmp_res['authors'] = []
            authors = response.get('author', [])
            for author in authors:
                author = author.get('given', '')+' '+author.get('family', '')
                tmp_res['authors'].append(author)
        
        result.append(tmp_res)
    
    return result
            



def abstract_collector(abstracts_dir_path, output_dir_path, download=False, download_list='', prefix_reference_file='', verbose=False):
    '''
    Gets abstracts from the given path.

    Arguments:
    
        abstracts_dir_path (str): Path from where to look for abstracts.
        output_dir_path (str): Path to save result files.
        download (bool): Indicates wether to download abstracts or not. Defaults to False.
        download_list (str): Path to csv file with download list. Defaults to empty string.
        prefix_reference_file (str): Path to prefix reference file for DOIs.
        verbose (bool): verbose. Defaults to False.
    
    Returns:

        list: Resulting list of cleaned abstracts.
    '''

    if download:
        get_abstracts(download_list, abstracts_dir_path, prefix_reference_file, verbose)

    data_abstracts = []

    data_length = 0
    if download and verbose:
        relevant_publishers = get_list_of_publishers(download_list, output_dir_path, prefix_reference_file, verbose)

    print('Cleaning abstracts...')

    for journal_dir in os.listdir(abstracts_dir_path):
        if(not os.path.isdir(os.path.join(abstracts_dir_path, journal_dir))): continue
        if verbose: print(journal_dir)
        cur_dir = os.path.join(abstracts_dir_path, journal_dir)
        for filetype_subdir in os.listdir(cur_dir):
            if verbose: print(filetype_subdir)
            cur_dir = os.path.join(cur_dir, filetype_subdir)
            counter = 0
            data_length = len(os.listdir(cur_dir))
            for filename in os.listdir(cur_dir):
                file_path = os.path.join(cur_dir, filename)
                doi = filepath_to_doi(file_path)
                abstract_text = ''

                if verbose: print(f'{counter+1}/{data_length+1}: {doi}')

                if (filetype_subdir == 'xml'):
                    abstract_xml = ET.parse(file_path)
                    xml_root = abstract_xml.getroot()
                
                    namespace_map = {'dc': 'http://purl.org/dc/elements/1.1/'}
                    abstract_text = xml_root.find('.//dc:description', namespaces=namespace_map)

                    if(abstract_text is not None and abstract_text.text is not None):
                        abstract_text = abstract_text.text
                
                elif (filetype_subdir == 'json'):
                    abstract_json = None
                    with open(file_path, 'r') as file:
                        abstract_json = json.load(file)
                    
                    if ('records' in abstract_json
                        and isinstance(abstract_json['records'], list)
                        and abstract_json['records']
                    ):
                        first_record = abstract_json['records'][0]
                        
                        if 'abstract' in first_record:
                            abstract_text = first_record['abstract']
                
                elif (filetype_subdir == 'txt'):
                    file_contents = ''
                    with open(file_path, 'r') as file:
                        file_contents = file.read()
                    
                    # match = re.search(r'(ABSTRACT|Abstract|SUMMARY|Summary).*?(REFERENCES|References|$)', file_contents, re.DOTALL)
                    # if match:
                        # abstract_text = match.group(0)
                    abstract_text = file_contents

                if(abstract_text and len(abstract_text)):
                    abstract_text = cleaning_pipe(abstract_text)
                    data_abstracts.append({'doi': doi, 'abstract': abstract_text})
                    counter+=1
            
            cur_dir = os.path.join(abstracts_dir_path, journal_dir)
    
    return data_abstracts

def information_extraction_pipeline(abstracts, verbose=False):
    '''
    Extracts nformation from clean abstracts.

    Argumetns:

        abstracts (list): list of abstracts.
        vebose (bool): verbose. Defaults to False.
    
    Returns:

        list: List of dictionaries with extracted data from rules and NLP.
    '''
    result = []
    data_length = len(abstracts)
    
    print('Extracting information...')

    for i in range(data_length):
        item = abstracts[i]
        doi = item['doi']
        abstract = item['abstract']
        tmp_res = {'doi': doi}

        if verbose: print(f'{i+1}/{data_length+1}: {doi}')
        
        if verbose: print('Rules...')
        
        epoch_numbers = rules.get_epochs_number(abstract)
        tmp_res['epoch_numbers'] = epoch_numbers

        epoch_names = rules.get_epoch_name(abstract, epoch_numbers)
        tmp_res['epochs'] = epoch_names

        if verbose: print('NLP...')
        named_entities = rules.named_entity_bert(abstract)
        locations = filter_named_entities(named_entities, 'LOC')
        
        tmp_res['locations'] = locations

        result.append(tmp_res)
    
    return result

def main(abstracts_dir_path, output_dir_path, output_file_path, download_abstracts, abstract_download_list, prefix_reference_file, verbose=False):
    '''
    main function of pipeline. Saves result of the pipeline and returns the result.

    Arguments:

        abstracts_dir_path (str): Path from where to read abstracts (if download, where to download abstracts first).
        output_dir_path (str): Path where to the result files
        output_file_path (str): Path where to save the result of the pipeline.
        download_abstracts (bool): Wether to download or no abstracts.
        abstract_download_list (str): Path to list of abstracts to download (csv with doi column).
        prefix_reference_file (str): Path to prefix reference file for DOIs.
        verbose (bool): verbose. Defaults to False.
    
    Returns:

        pandas.DataFrame: the result of the pipeline.
    '''

    load_dotenv()
    time_spent = {
        'abstract_collections': 0,
        'metadata_collection': 0,
        'information_extraction': 0
    }
    start_time = 0
    end_time = 0

    if verbose: start_time = time.time()
    abstracts = abstract_collector(abstracts_dir_path, output_dir_path, download_abstracts, abstract_download_list, prefix_reference_file, verbose)
    if verbose: end_time = time.time()
    if verbose: time_spent['abstract_collections'] = end_time-start_time

    if verbose: start_time = time.time()
    metadata = metadata_collector(abstracts, verbose)
    if verbose: end_time = time.time()
    if verbose: time_spent['metadata_collection'] = end_time-start_time

    if verbose: start_time = time.time()
    extracted = information_extraction_pipeline(abstracts, verbose)
    if verbose: end_time = time.time()
    if verbose: time_spent['information_extraction'] = end_time-start_time

    if verbose:
        print('Abstract collection stage time:\t', time_spent['abstract_collections'])
        print('Metadata collection stage time:\t', time_spent['metadata_collection'])
        print('Information extraction stage time:\t', time_spent['information_extraction'])

    metadata = pd.DataFrame(metadata)
    extracted = pd.DataFrame(extracted)
    result = pd.merge(metadata, extracted, on='doi', how='inner')
    result.to_csv(output_file_path, index=False)

    return result

if __name__ == '__main__':
    '''
    Entrypoint of the pipeline script.

    Arguments:

        abstracts_path: Location from where to read abstracts (where to download if download).
        output_dir_path: Where to save the result.
        download_abstracts: Optional. --download flag, to first download abstracts.
        abstract_download_list: Optional. If --download, the list of DOIs.
        verbose: Optional. -v flag for Verbose.
    '''

    if(len(sys.argv)>1 and sys.argv[1]=='-h'):
        print(f"python {os.path.basename(sys.argv[0])} [OPTIONS] [PATHS]")
        print("OPTIONS:")
        print(f"\t --download \t to download abstracts from the Internet")
        print(f"\t -v \t\t to show verbose")
        print("PATHS:")
        print(f"\t <abstracts_path> \t path to a directory with abstracts")
        print(f"\t <output_path> \t path to save results")
        print(f"\t <doi_prefix_reference_path> \t path to a .csv file with DOI prefix references")
        print(f"\t <download_list_path> \t path to a .csv file with DOIs to be downloaded")
        sys.exit(0)

    download_abstracts = '--download' in sys.argv
    if download_abstracts:
        sys.argv.remove('--download')
    
    verbose = '-v' in sys.argv
    if verbose:
        sys.argv.remove('-v')

    abstracts_dir_path = ''
    output_file_path = ''
    output_dir_path = ''
    abstract_download_list = ''
    prefix_reference_file = ''

    if(len(sys.argv)>1):
        abstracts_dir_path = os.path.abspath(sys.argv[1])
    else:
        abstracts_dir_path = os.makedirs(os.path.abspath(os.path.join(os.path.dirname(__file__), '../target/abstracts/'), exist_ok = True))
    
    if(len(sys.argv)>2):
        output_dir_path = os.path.abspath(sys.argv[2])
        if(os.path.isdir(output_dir_path)):
            output_file_path = os.path.join(output_dir_path, 'pipe_result.csv')
        else:
            output_file_path = output_dir_path
            output_dir_path = os.path.dirname(output_file_path)
    else:
        output_dir_path = os.makedirs(os.path.abspath(os.path.join(os.path.dirname(__file__), '../results/target/'), exist_ok = True))
        output_file_path = os.path.join(output_dir_path, 'pipe_result.csv')

    if download_abstracts or verbose:
        if len(sys.argv) > 3:
            prefix_reference_file = os.path.abspath(sys.argv[3])
        else:
            prefix_reference_file = os.path.join(abstracts_dir_path, 'doi-prefix-publishers.csv')

    if download_abstracts:
        if len(sys.argv) > 4:
            abstract_download_list = os.path.abspath(sys.argv[4])
        else:
            abstract_download_list = os.path.join(abstracts_dir_path, 'download_list.csv')
    
    main(abstracts_dir_path, output_dir_path, output_file_path, download_abstracts, abstract_download_list, prefix_reference_file, verbose)

    
