.. NLP004 documentation master file, created by
   sphinx-quickstart on Tue Nov 28 17:44:39 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NLP004's documentation!
==================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   modules
   pipe

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
